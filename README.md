# TP noté sécurité des réseaux de neurones

Lukas PRESENCIA & Alexis VIVIER - Master informatique Ynov Bordeaux

## Réponses aux questions

### Préparer l’environnement d’analyse

<!-- ![image info](./assets/mouje10.png) -->

1 - TANNER est un composant utilisé dans le pot de miel T-Pot. Quelle est la fonction de ce composant ?

`TANNER est un service d'analyse et de classification de données à distance, pour évaluer les requêtes HTTP et composer la réponse ensuite servie par les événements SNARE.`

2 - Prouvez que les données ont été correctement chargées dans ElasticSearch en copiant la ligne appropriée de la liste des informations concernant les indices d’un cluster ElasticSearch (voir documentation).

![image info](./assets/kalus1.png)

### Analyse et qualification

#### Première partie

1 - Quelle est l’adresse IP ayant réalisé le plus d’activités ?

`L'IP ayant enregistré le plus d'activités est : 52.156.59.22 avec 346 hits.`

2 - Faite une recherche Whois sur l’adresse IP. Qui gère cette adresse IP ?

```
52.156.59.22 was found in our database!

This IP was reported 4 times. Confidence of Abuse is 10%:
?
10%
ISP 	Microsoft Corporation
Usage Type 	Data Center/Web Hosting/Transit
Domain Name 	microsoft.com
Country 	Japan
City 	Tokyo, Tokyo
```

3 - Donner l’adresse IP ciblée par l’adresse IP identifiée précédemment (indice : regarder les entêtes)

```
L'adresse IP ciblée par 52.156.59.22 est : 167.172.104.173
```

4 - Est-ce-que cette adresse IP est malveillante ? Si oui, prouvez-le grâce aux recherches OSINT.

```
Donc non cette IP n'est pas malveillante car elle n'a jamais été reported.
```

5 - Quelle ressource a été recherchée par l’attaquant ? (attaque de type "File names bruteforcing")

```
L'attaquant chercher à accèder a /PHP/eval-stdin.php.
```

6 - Une vulnérabilité existe concernant la ressource recherchée. Donnez le CVE de la faille en question.

```
Cela fait référence à la CVE-2017-9841. Cela permet aux attaquants distants d'exécuter du code PHP arbitraire via les données HTTP POST commençant par une sous-chaîne "<? Php", comme démontré par une attaque sur un site avec un dossier / vendor exposé, c'est-à-dire un accès externe à l'URI /vendor/phpunit/phpunit/src/Util/PHP/eval-stdin.php
```

7 - Quel est le type de vulnérabilité correspondant à cette CVE ?

#### Deuxième partie en filtrant sur 87.231.103.55

1 - Quelle ressource a été ciblée par l’adresse IP ?

```
/tmpfs/auto.jpg?usr=guest&pwd=123456789
```

2 - A quoi correspond cette ressource ?

```
C'est la plateforme d'administration d'une caméra IP. L'attaquant tente donc de s'y connecter afin d'accèder 
à l'interface d'administration de la caméra
```

3 - Quel est le type d’attaque réalisé par l’attaquant ? (Vous pouvez l’expliquer en français)

```
C'est une attaque de type bruteforce car on peut voir qu'il tente toujours la même requete
en changeant le mot de passe et en laissant l'user.
```


### Partie IA

1 - Dans votre notebook jupyter, prouvez en faisant 10 prédiction successives que l’implémentation ci-dessus est incorrecte.

```
L'algorythme ne marche pas car le modèle ne retourne pas la table de vérité du modele.
```

2 - Par rapport au dataset utilisé, quel serait le solver à privilégier et pourquoi ? (indice : lire la documentation)

```
La gradiant descent est la plus optimale car c'est avec elle seule que nous avons trouvé les bonnes prédictions
```
![image info](./assets/kalus2.png)

3 - Dans votre notebook jupyter, en utilisant la fonction d’activation sigmoïde, après avoir modifié l’implémentation ci-dessus comme demandé dans la consigne principale, prouvez en faisant 10 prédictions successives que la nouvelle implémentation est correcte.